def read_input(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    people = []
    for line in lines:
        parts = line.split()
        name = parts[0]
        marks = list(map(int, parts[1:]))
        people.append((name, marks))
    return people

def compare_marks(marks1, marks2):
    for m1, m2 in zip(marks1, marks2):
        if m1 <= m2:
            return False
    return True

def rank_people(people):
    ranked_lists = []
    for person in people:
        inserted = False
        for rank_list in ranked_lists:
            if compare_marks(person[1], rank_list[-1][1]):
                rank_list.append(person)
                inserted = True
                break
            elif compare_marks(rank_list[0][1], person[1]):
                rank_list.insert(0, person)
                inserted = True
                break
            else:
                for i in range(len(rank_list) - 1):
                    if compare_marks(person[1], rank_list[i][1]) and compare_marks(rank_list[i + 1][1], person[1]):
                        rank_list.insert(i + 1, person)
                        inserted = True
                        break
            if inserted:
                break
        if not inserted:
            ranked_lists.append([person])
    return ranked_lists

def format_output(ranked_lists):
    result = []
    for rank_list in ranked_lists:
        if len(rank_list) > 1:
            result.append("<".join([person[0] for person in rank_list]))
        else:
            result.append("#" + rank_list[0][0])
    return "    ".join(result)


file_path = "studentRanking.txt"  
people = read_input(file_path)
ranked_lists = rank_people(people)
output = format_output(ranked_lists)
print(output)

