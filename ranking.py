from itertools import combinations as nCr

def sorting(l1: list, l2: list) -> str:
    if all(a > b for a, b in zip(l1[1:], l2[1:])):
        return f"{l1[0]} > {l2[0]}"
    elif all(b > a for a, b in zip(l1[1:], l2[1:])):
        return f"{l2[0]} > {l1[0]}"
    else:
        return "0"


input = [["A", 12, 14, 16], ["B", 5, 6, 7], ["C", 17, 20, 23], ["D", 2, 40, 12], ["E", 3, 41, 13], ["F", 7, 8, 9], ["G", 4, 5, 6]]
l =[]
for a, b in nCr(input, 2):
    l.append(sorting(a, b))

print(l)



